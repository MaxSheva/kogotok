<?php

namespace App\Http\Sections;

use AdminColumn;
use AdminDisplay;
use AdminForm;
use AdminFormElement;
use SleepingOwl\Admin\Contracts\Display\DisplayInterface;
use SleepingOwl\Admin\Contracts\Form\FormInterface;
use SleepingOwl\Admin\Section;
use AdminColumnEditable;

/**
 * Class Services
 *
 * @property \App\Service $model
 *
 * @see http://sleepingowladmin.ru/docs/model_configuration_section
 */
class Services extends Section
{

    /**
     * @see http://sleepingowladmin.ru/docs/model_configuration#ограничение-прав-доступа
     *
     * @var bool
     */
    protected $checkAccess = false;

    /**
     * @var string
     */
    protected $title = 'Услуги';

    /**
     * @var string
     */
    protected $alias = 'services';

    /**
     * @return DisplayInterface
     */
    public function onDisplay()
    {
        $display = AdminDisplay::datatables();
        $display->setHtmlAttribute('class', 'table-primary table-hover');

        $display->with('category');

        $display->setColumns(
            AdminColumn::text('id')->setLabel('#'),
            AdminColumn::text('category.name')->setLabel('Категория'),
            AdminColumnEditable::text('name')->setLabel('Название'),
            AdminColumnEditable::text('price')->setLabel('Цена, грн'),
            AdminColumnEditable::text('order')->setLabel('№ п/п'),
            AdminColumnEditable::checkbox('published')->setLabel('Show')
        );

        return $display;

    }

    /**
     * @param int $id
     *
     * @return FormInterface
     */
    public function onEdit($id)
    {
        return AdminForm::panel()
            ->addBody([
                AdminFormElement::checkbox('published')->setLabel('Публиковать'),
                AdminFormElement::number('order')->setLabel('Очерёдность')->required('Поле обязательно для заполнения')->setDefaultValue(0),
                AdminFormElement::text('name')->setLabel('Название')->required('Поле обязательно для заполнения'),
                AdminFormElement::text('slug')->setLabel('Латиницей')->required('Поле обязательно для заполнения')->setHelpText('в нижнем регистре (маленькими латинскими буквами)'),

                AdminFormElement::select('category_id')->setLabel('Категория')
                    ->setModelForOptions(\App\Category::class)
                    ->setLoadOptionsQueryPreparer(function ($item, $query) {
                        $query->published(); // активные категории (им можно присваивать услуги)
                        return $query;
                    })
                    ->setDisplay("name")
                    ->setHelpText('Выберите категорию, к которой пренадлежит товар')
                    ->required('Поле обязательно для заполнения'),

                AdminFormElement::textarea('desc')->setLabel('Описание')->setHelpText('краткое описание услуги или особенностей услуги'),
                AdminFormElement::text('before_price')->setLabel('Перед ценой')->setDefaultValue('')->setHelpText('если перед ценой нужно вставить какой-то текст, например частичку "от"'),
                AdminFormElement::number('price')->required('Поле обязательно для заполнения')->setLabel('Цена, грн')
            ]);

    }

    /**
     * @return FormInterface
     */
    public function onCreate()
    {
        return $this->onEdit(null);
    }

    /**
     * @return void
     */
    public function onDelete($id)
    {
        // remove if unused
    }

    /**
     * @return void
     */
    public function onRestore($id)
    {
        // remove if unused
    }
}
