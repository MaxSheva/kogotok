<?php

use Illuminate\Database\Seeder;

class TestimonialsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // В database\factories\ModelFactory.php находится фабрика создания Producer
        factory(\App\Testimonial::class, 10)->create();
    }
}
