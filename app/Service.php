<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Service
 *
 * @property int $id
 * @property int $category_id id категори услуги
 * @property string $name Название услуги
 * @property string $slug ЧПУ услуги
 * @property string|null $desc Описание услуги
 * @property int $price Цена услуги
 * @property int $order Очерёдность вывода услуги
 * @property int $published Отображать на сайте
 * @property-read \App\Category $category
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Photo[] $photos
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Service ordered()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Service published()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Service whereCategoryId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Service whereDesc($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Service whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Service whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Service whereOrder($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Service wherePrice($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Service wherePublished($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Service whereSlug($value)
 * @mixin \Eloquent
 */
class Service extends Model
{
    public $timestamps = false;

    protected $fillable = [
        'name',
        'slug',
        'desc',
        'price',
        'order',
        'published',
    ];

    // scopes
    public function scopePublished($query)
    {
        return $query->where('published', true);
    }

    public function scopeOrdered($query)
    {
        return $query->orderBy('order');
    }


    // relations

    /**
     * Get the category that owns the service
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function category()
    {
        return $this->belongsTo(Category::class);
    }

    /**
     * The photos that belong to the service
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function photos()
    {
        return $this->belongsToMany(Photo::class);
    }
}
