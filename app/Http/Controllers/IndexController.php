<?php

namespace App\Http\Controllers;

use App\Testimonial;
use Illuminate\Http\Request;

class IndexController extends Controller
{
    public function index()
    {
        // 5 случайных отзывов
        $testimonials = Testimonial::published()->inRandomOrder()->limit(5)->get();

        return view('index', compact('testimonials'));
    }
}
