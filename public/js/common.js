$(document).ready(function () {

    // menu open
    $(".top_panel__menu__btn").click(function () {
        $(this).toggleClass("active");
        $('.top_panel__menu__list').toggleClass("active");
        // $('#header').toggleClass("active");
    });


    // main scroll
    $(".picture__main-scroll").click(function () {
        Wheight = window.innerHeight;
        $("body, html").animate({
            scrollTop: Wheight
        }, 800);
        return false;
    });


    // carousel testimonials
    $('.owl-carousel').owlCarousel({
        // loop:true,   // перескок с последней на первую и наоборот
        margin: 10,
        center: true,
        startPosition: 1,
        autoplay: true,
        autoplayTimeout: 6000,
        rewind: true,
        responsiveClass: true,
        responsive: {
            0: {
                items: 1,
                nav: false
            },
            600: {
                items: 3,
                nav: false
            },
            1000: {
                items: 5,
                nav: true,
                loop: false
            }
        }
    });


    // анимация перехода по ссылке к якорю
    $("a[href='#testimonials']").click(function (event) {
        // отменяем стандартную обработку нажатия по ссылке
        event.preventDefault();
        // забираем идентификатор блока с атрибута href
        var id = $(this).attr('href'),

            //узнаем высоту от начала страницы до блока на который ссылается якорь
            top = $(id).offset().top;

        // анимируем переход на расстояние - top за 1200 мс
        $('body,html').animate({scrollTop: top}, 1200);
    });


    // аккордеон (услуги)
    // скрываем все блоки с описанием услуги
    $(".service_description").hide();

    // Услуги показать/скрыть описание услуги при клике
    $(".service_head").click(function () {
        $(this).next(".service_description").slideToggle();
    });


    // портфолио -> фильтры - добавить класс active (адрес ссылки = текущему url)
    var url = document.documentURI;
    $("[href='" + url + "']").addClass('active');


    // портфолио показать/скрыть стоимость услуг
    // показываем стоимость услуг при клике на фото
    $(".touch_icon").click(function () {
        $(this).next(".services_cost").addClass('active');
    });

    // скрываем стоимость услуг при клике на блок со стоимостью услуг
    $(".services_cost").click(function () {
        $(this).removeClass('active');
    });



    // popup Главная -> Обо мне -> Качество -> безопасность
    $(function () {
        $('.popup-modal').magnificPopup({
            type: 'inline',
            closeOnContentClick: true
        });
    });

});

