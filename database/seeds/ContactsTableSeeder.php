<?php

use Illuminate\Database\Seeder;

class ContactsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \App\Contact::create([
            'type' => 'телефон',
            'icon' => 'fa fa-mobile',
            'data' => '+38 (050) 555-55-55',
            'order' => 1,
            'published' => 1
        ]);

        \App\Contact::create([
            'type' => 'город',
            'icon' => 'fa fa-map-marker',
            'data' => 'Киев',
            'order' => 2,
            'published' => 1
        ]);
    }
}
