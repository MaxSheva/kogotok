<?php

namespace App\Http\Sections;

use AdminColumn;
use AdminDisplay;
use AdminForm;
use AdminFormElement;
use App\Photo;
use Illuminate\Support\Facades\File;
use SleepingOwl\Admin\Contracts\Display\DisplayInterface;
use SleepingOwl\Admin\Contracts\Form\FormInterface;
use SleepingOwl\Admin\Section;

/**
 * Class Photo
 *
 * @property \App\Photos $model
 *
 * @see http://sleepingowladmin.ru/docs/model_configuration_section
 */
class Photos extends Section
{
    /**
     * @see http://sleepingowladmin.ru/docs/model_configuration#ограничение-прав-доступа
     *
     * @var bool
     */
    protected $checkAccess = false;

    /**
     * @var string
     */
    protected $title = 'Фото';

    /**
     * @var string
     */
    protected $alias = 'photos';

    /**
     * @return DisplayInterface
     */
    public function onDisplay()
    {
        $display = AdminDisplay::datatables();
        $display->setHtmlAttribute('class', 'table-primary table-hover');

        $display->setColumns(
            AdminColumn::text('id')->setLabel('#'),
            AdminColumn::image('img')->setLabel('Фото')
        );

        $display->setOrder([[0, 'desc']]);

        return $display;
    }

    /**
     * @param int $id
     *
     * @return FormInterface
     */
    public function onEdit($id)
    {
        return AdminForm::panel()
            ->addBody([
                AdminFormElement::image('img')->setUploadPath(function () {
                    return 'images/uploads/portfolio';
                })->setLabel('Фото')->required('Поле обязательно для заполнения'),


                AdminFormElement::multiselect('services')->setLabel('Услуги')
                    ->setModelForOptions(\App\Service::class)
                    ->setLoadOptionsQueryPreparer(function ($item, $query) {
                        $query->published(); // активные услуги
                        return $query;
                    })
                    ->setDisplay("name")
                    ->setHelpText('Выберите услуги, которые отображены на фото'),

                AdminFormElement::multiselect('filters')->setLabel('Фильтры')
                    ->setModelForOptions(\App\Filter::class)
                    ->setLoadOptionsQueryPreparer(function ($item, $query) {
                        $query->published(); // активные услуги
                        return $query;
                    })
                    ->setDisplay("name")
                    ->setHelpText('Выберите фильтры, которым удовлетворяет фото'),
            ]);
    }

    /**
     * @return FormInterface
     */
    public function onCreate()
    {
        return $this->onEdit(null);
    }

    /**
     * @return void
     */
    public function onDelete($id)
    {
        $photo_img = Photo::find($id)->getAttribute('img');

        if(File::delete($photo_img)){
            return (true);
        };

    }

    /**
     * @return void
     */
    public function onRestore($id)
    {
        // remove if unused
    }
}
