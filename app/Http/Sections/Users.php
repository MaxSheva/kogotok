<?php

namespace App\Http\Sections;

use AdminColumn;
use AdminDisplay;
use AdminDisplayFilter;
use AdminForm;
use AdminFormElement;
use App\User;
use SleepingOwl\Admin\Contracts\Display\DisplayInterface;
use SleepingOwl\Admin\Contracts\Form\FormInterface;
use SleepingOwl\Admin\Contracts\Initializable;
use SleepingOwl\Admin\Section;
use AdminColumnEditable;


/**
 * Class User
 *
 * @property \App\User $model
 *
 * @see http://sleepingowladmin.ru/docs/model_configuration_section
 */
class Users extends Section implements Initializable
{
    /**
     * @see http://sleepingowladmin.ru/docs/model_configuration#ограничение-прав-доступа
     *
     * @var bool
     */
    protected $checkAccess = true;

    /**
     * @var string
     */
    protected $title = 'Пользователи';

    /**
     * Если класс реализует интерфейс SleepingOwl\Admin\Contracts\Initializable, то автоматически будет вызван метод initialize, в котром можно поместить код, который нужно выполнить после регистрации класса, например добавление раздела в меню
     * Initialize class.
     */
    public function initialize()
    {
        // Добавление пункта меню, счетчика кол-ва записей в разделе, иконки
        $this->addToNavigation($priority = 100)
            ->setIcon('fa fa-users');
    }

    /**
     * @var string
     */
    protected $alias = 'users';

    /**
     * @return DisplayInterface
     */
    public function onDisplay()
    {
        $display = AdminDisplay::datatables();

        if (auth()->user()->is_admin == false) {
            $display->setFilters(
                AdminDisplayFilter::field('is_admin')->setValue(0)
            );
        }

        $display->setHtmlAttribute('class', 'table-primary table-hover')
            ->setColumns(
                AdminColumnEditable::text('name')->setLabel('Имя'),
                AdminColumn::email('email')->setLabel('E-mail')
            );
        return $display;
    }

    /**
     * @param int $id
     *
     * @return FormInterface
     */
    public function onEdit($id)
    {
        if (auth()->user()->is_admin) {
            return AdminForm::panel()->addBody(
                AdminFormElement::text('name')->setLabel('Имя')->required('Поле обязательно для заполнения'),
                AdminFormElement::text('email')->setLabel('E-mail')->required('Поле обязательно для заполнения')->unique('Поле должно быть уникальным'),
                AdminFormElement::password('password')->setLabel('Пароль')->required('Поле обязательно для заполнения')->hashWithBcrypt(),
                AdminFormElement::checkbox('is_admin')->setLabel('Админ')
            );
        } else {
            return AdminForm::panel()->addBody(
                AdminFormElement::text('name')->setLabel('Имя')->required('Поле обязательно для заполнения'),
                AdminFormElement::text('email')->setLabel('E-mail')->required('Поле обязательно для заполнения')->unique('Поле должно быть уникальным'),
                AdminFormElement::password('password')->setLabel('Пароль')->required('Поле обязательно для заполнения')->hashWithBcrypt()
            );
        }
    }

    /**
     * @return FormInterface
     */
    public function onCreate()
    {
        return $this->onEdit(null);
    }

    /**
     * @return void
     */
    public function onDelete($id)
    {
        // remove if unused
    }

    /**
     * @return void
     */
    public function onRestore($id)
    {
        // remove if unused
    }
}
