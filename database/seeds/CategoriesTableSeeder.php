<?php

use Illuminate\Database\Seeder;

class CategoriesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // В database\factories\ModelFactory.php находится фабрика создания Category
        factory(\App\Category::class, 6)->create()
            // для каждой категории создаём от 2 до 5 услуг
            ->each(function ($category) {
                $category->services()
                    ->saveMany(factory(App\Service::class, random_int(2, 5))
                        ->create([
                            'category_id' => $category->id,
                        ])
                    );
            });
    }
}
