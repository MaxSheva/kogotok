<?php

namespace App\Http\Sections;

use AdminColumn;
use AdminDisplay;
use AdminForm;
use AdminFormElement;
use SleepingOwl\Admin\Contracts\Display\DisplayInterface;
use SleepingOwl\Admin\Contracts\Form\FormInterface;
use SleepingOwl\Admin\Section;
use AdminColumnEditable;

/**
 * Class Categories
 *
 * @property \App\Category $model
 *
 * @see http://sleepingowladmin.ru/docs/model_configuration_section
 */
class Categories extends Section
{
    /**
     * @see http://sleepingowladmin.ru/docs/model_configuration#ограничение-прав-доступа
     *
     * @var bool
     */
    protected $checkAccess = false;

    /**
     * @var string
     */
    protected $title = 'Категории';

    /**
     * @var string
     */
    protected $alias = 'categories';

    /**
     * @return DisplayInterface
     */
    public function onDisplay()
    {
        $display = AdminDisplay::datatables();
        $display->setHtmlAttribute('class', 'table-primary table-hover');
//        $display->setApply(function ($query) {  //сортировка при выводе на экран
//            $query->orderBy('order', 'asc');
//        });
        $display->setColumns([
            AdminColumnEditable::text('id')->setLabel('#'),
            AdminColumnEditable::text('name')->setLabel('Название'),
            AdminColumnEditable::text('order')->setLabel('№ п/п'),
            AdminColumnEditable::checkbox('published')->setLabel('Show')
        ]);

        return $display;
    }

    /**
     * @param int $id
     *
     * @return FormInterface
     */
    public function onEdit($id)
    {
        return AdminForm::panel()
            ->addBody([
                AdminFormElement::checkbox('published')->setLabel('Публиковать'),
                AdminFormElement::number('order')->setLabel('Очерёдность')->required('Поле обязательно для заполнения')->setDefaultValue(0),
                AdminFormElement::text('name')->setLabel('Название')->required('Поле обязательно для заполнения'),
                AdminFormElement::text('slug')->setLabel('Латиницей')->required('Поле обязательно для заполнения')->setHelpText('в нижнем регистре (маленькими латинскими буквами)'),
            ]);
    }

    /**
     * @return FormInterface
     */
    public function onCreate()
    {
        return $this->onEdit(null);
    }

    /**
     * @return void
     */
    public function onDelete($id)
    {
        // remove if unused
    }

    /**
     * @return void
     */
    public function onRestore($id)
    {
        // remove if unused
    }
}
