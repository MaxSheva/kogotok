<?php

namespace App\Providers;

use App\Contact;
use Illuminate\Support\Facades\View;
use Illuminate\Support\ServiceProvider;

class ShareAllViewServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {

            $phone = Contact::published()->where('type', 'like', 'тел%')->first();

            if ($phone){
                $phone_numeric = preg_replace('/[^0-9]/', '', $phone->data);
            } else {
                $phone_numeric = 0;
            }

            $city = Contact::published()->where('type', 'like', 'гор%')->first();


            View::share(compact('phone', 'phone_numeric', 'city', 'status'));

    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
