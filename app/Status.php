<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Status
 *
 * @property int $id
 * @property string $img Картинка перед текстом статуса
 * @property string $text Текст статуса
 * @property int $published Отображать на сайте
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Status published()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Status whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Status whereImg($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Status wherePublished($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Status whereText($value)
 * @mixin \Eloquent
 */
class Status extends Model
{
    public $timestamps = false;

    protected $fillable = [
        'img',
        'text',
        'published',
    ];

    public function scopePublished($query)
    {
        return $query->where('published', true);
    }

}
