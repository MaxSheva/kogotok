<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // $this->call(UsersTableSeeder::class);

        // последовательсть вызовов имеет значение
        $this->call(UsersTableSeeder::class);
        $this->call(ContactsTableSeeder::class);
        $this->call(StatusesTableSeeder::class);
        $this->call(TestimonialsTableSeeder::class);
        $this->call(CategoriesTableSeeder::class);      // with creating and binding services
        $this->call(FiltersTableSeeder::class);
        $this->call(PhotosTableSeeder::class);          // with binding services and filters

    }
}
