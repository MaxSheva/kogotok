<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//Route::get('/', function () {
//    return view('welcome');
//});


/* Главная страница */

Route::get('/', 'IndexController@index')->name('main');

/* ************************************************* END ************************************************************ */


/* Услуги и цены */

Route::get('/servises', 'ServiceController@index')->name('services');

/* ************************************************* END ************************************************************ */


/* Портфолио */

Route::get('/portfolio/{filter_slug?}', 'PortfolioController@index')->name('portfolio');

/* ************************************************* END ************************************************************ */
Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
