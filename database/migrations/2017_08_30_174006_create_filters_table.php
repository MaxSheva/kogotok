<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFiltersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('filters', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name')->unique()->required()->comment('Название фильтра');
            $table->string('slug')->unique()->required()->comment('ЧПУ фильтра');
            $table->integer('order')->unsigned()->default(0)->comment('Порядок вывода фильтров');
            $table->boolean('published')->comment('Отображать на сайте');
        });

        // связная таблица
        Schema::create('filter_photo', function (Blueprint $table) {
            $table->integer('filter_id')->unsigned();
            $table->foreign('filter_id')->references('id')->on('filters')->onDelete('cascade');
            $table->integer('photo_id')->unsigned();
            $table->foreign('photo_id')->references('id')->on('photos')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('filter_photo');
        Schema::dropIfExists('filters');
    }
}
