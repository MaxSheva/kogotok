@extends('layout')

@section('title')
    Kogotok | Портфолио
@endsection

@section('content')

    @include('analyticstracking')

    @include('top_panel')

    <!-- PORTFOLIO -->
    <section id="portfolio" class="flex">

        <header class="portfolio__header flex">
            <h1>ПОРТФОЛИО</h1>
            <div class="short_line"></div>
        </header>

        <!-- filters -->
        <div class="filters flex content_padding">

            <div class="filters__item"><a href="{{ route('portfolio') }}" class="filters__link">ПОКАЗАТЬ ВСЕ</a></div>

            @foreach($filters as $filter)
                <div class="filters__item">
                    <a href="{{ route('portfolio', ['filter_slug' => $filter->slug]) }}" class="filters__link"
                       name="{{ $filter->slug }}">{{ mb_strtoupper($filter->name) }}</a>
                </div>
            @endforeach

        </div>
        <!-- /filters -->


        @foreach($photos as $photo)
            {{-- single photo --}}
            <div class="img_wrapper flex">

                <!-- touch_icon -->
                <span class="touch_icon content_padding flex"><i class="fa fa-bullseye fa-lg"></i></span>
                <!-- /touch_icon -->

                <!-- services_cost -->
                <div class="services_cost flex content_padding">

                    {{--если к фото прикрепили услуги - выводим данные и ВСЕГО --}}
                    @if(count($photo->services))
                        @foreach($photo->services as $service)
                            {{-- services --}}
                            <div class="services_cost__service flex">
                                <div class="services_cost__service__name">
                                    <i class="fa fa-check"></i>
                                    <span>&nbsp;{{ $service->category->name .' '. $service->name }}</span>
                                </div>
                                {{--<div class="service_price">--}}
                                {{--<i>{{ $service->before_price }}</i>--}}
                                {{--<span>{{ $service->price }} грн</span>--}}
                                {{--</div>--}}
                            </div>
                            <!-- /services -->
                        @endforeach

                        <div class="short_line_white"></div>

                        {{--<!-- total_cost -->--}}
                        {{--<div class="total_cost flex">--}}
                        {{--<div class="total_title">--}}
                        {{--<span>&nbsp;ВСЕГО</span>--}}
                        {{--</div>--}}
                        {{--<div class="total_price">--}}
                        {{--<span>{{ $photo->services->sum('price') }} грн</span>--}}
                        {{--</div>--}}
                        {{--</div>--}}
                        {{--<!-- /total_cost -->--}}
                        {{--если к фото не прикрепили услуги - выводим альтернативный текст--}}
                    @else
                        <div class="services_cost__alternative_text">Подробности уточняйте у мастера</div>
                    @endif

                </div>
                <!-- /services_cost -->

                <!-- photos -->
                <img src="{{ asset($photo->img) }}" alt="manicure">
                <!-- /photos -->

            </div>
            {{-- /single photo --}}
        @endforeach

    </section>
    <div class="loader">Loading...</div>
    <div class="portfolio__more_works content_padding"><a href="https://www.instagram.com/bereztan/?hl=ru">смотреть больше работ в Instagram <i class="fa fa-instagram"></i></a></div>
    <!-- /PORTFOLIO -->

@endsection


@section('scripts')
    <script src="{{ asset('js/portfolio_lazy_load.js') }}"></script>
@endsection