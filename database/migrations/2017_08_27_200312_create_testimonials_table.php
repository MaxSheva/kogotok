<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTestimonialsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('testimonials', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name')->required()->comment('Имя пользователя');
            $table->string('img')->required()->comment('Фото пользователя');
            $table->string('link')->required()->comment('Ссылка на аккаут пользователя в Instagram');
            $table->text('text')->required()->comment('Отзыв');
            $table->boolean('published')->comment('Отображать на сайте');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('testimonials');
    }
}
