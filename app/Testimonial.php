<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Testimonial
 *
 * @property int $id
 * @property string $name Имя пользователя
 * @property string $img Фото пользователя
 * @property string $link Ссылка на аккаут пользователя в Instagram
 * @property string $text Отзыв
 * @property int $published Отображать на сайте
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Testimonial published()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Testimonial whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Testimonial whereImg($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Testimonial whereLink($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Testimonial whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Testimonial wherePublished($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Testimonial whereText($value)
 * @mixin \Eloquent
 */
class Testimonial extends Model
{
    public $timestamps = false;

    protected $fillable = [
        'name',
        'img',
        'link',
        'text',
        'published'
    ];

    public function scopePublished($query)
    {
        return $query->where('published', true);
    }
}
