<?php

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| Here you may define all of your model factories. Model factories give
| you a convenient way to create models for testing and seeding your
| database. Just tell the factory how a default model should look.
|
*/

/** @var \Illuminate\Database\Eloquent\Factory $factory */
$factory->define(App\User::class, function (Faker\Generator $faker) {
    static $password;

    return [
        'name' => $faker->name,
        'email' => $faker->unique()->safeEmail,
        'password' => $password ?: $password = bcrypt('secret'),
        'remember_token' => str_random(10),
    ];
});


// Status
$factory->define(\App\Status::class, function (Faker\Generator $faker) {
    return [
        'img' => 'images/uploads/statuses/default.png',
        'text' => $faker->text(25),
        'published' => $faker->boolean(100)
    ];
});


// Testimonial
$factory->define(\App\Testimonial::class, function (Faker\Generator $faker) {
    return [
        'name' => $faker->firstName('female'),
        'img' => 'images/uploads/testimonials/default.jpg',
        'link' => 'https://www.instagram.com/bereztan/?hl=ru',
        'text' => $faker->text(),
        'published' => $faker->boolean(100)
    ];
});


// Category
$factory->define(\App\Category::class, function (Faker\Generator $faker) {
    $category_name = $faker->unique()->word;
    return [
        'name' => ucfirst($category_name),
        'slug' => $category_name,
        'order' => $faker->unique()->randomNumber(),
        'published' => $faker->boolean(100)
    ];
});


// Service
$factory->define(\App\Service::class, function (Faker\Generator $faker) {
    $service_name = $faker->unique()->word;
    return [
//        'category_id' => $faker->randomNumber(1)
        'name' => ucfirst($service_name),
        'slug' => $service_name,
        'desc' => $faker->text(),
        'price' => $faker->numberBetween(50, 1000),
        'order' => $faker->unique()->randomNumber(),
        'published' => $faker->boolean(100)
    ];
});


// Filter
$factory->define(\App\Filter::class, function (Faker\Generator $faker) {
    // формируем случайную строку ограниченой длины от 7 до 14 символов
    $filter_name = mb_substr(md5(uniqid(rand())), rand(0, 5), rand(7, 14));
    return [
        'name' => $filter_name,
        'slug' => $filter_name,
        'order' => $faker->unique()->randomNumber(),
        'published' => $faker->boolean(100)
    ];
});


// Photo
$factory->define(\App\Photo::class, function (Faker\Generator $faker) {
    return [
        'img' => 'images/uploads/portfolio/default.jpg',
        'created_at' => $faker->dateTime()
    ];
});
