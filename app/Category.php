<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Category
 *
 * @property int $id
 * @property string $name
 * @property string $slug ЧПУ категории
 * @property int $order Очерёдность вывода категории
 * @property int $published Отображать на сайте
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Service[] $services
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Category ordered()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Category published()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Category whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Category whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Category whereOrder($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Category wherePublished($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Category whereSlug($value)
 * @mixin \Eloquent
 */
class Category extends Model
{
    public $timestamps = false;

    protected $fillable = [
        'name',
        'slug',
        'order',
        'published'
    ];

    // scopes
    public function scopePublished($query)
    {
        return $query->where('published', true);
    }

    public function scopeOrdered($query)
    {
        return $query->orderBy('order');
    }

    // relations

    /**
     * Get the services for the category.
     * @return $this
     */
    public function services()
    {
        return $this->hasMany(Service::class)->published()->ordered();
    }

}
