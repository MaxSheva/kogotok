<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Filter
 *
 * @property int $id
 * @property string $name Название фильтра
 * @property string $slug ЧПУ фильтра
 * @property int $order Порядок вывода фильтров
 * @property int $published Отображать на сайте
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Photo[] $photos
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Filter ordered()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Filter published()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Filter whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Filter whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Filter whereOrder($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Filter wherePublished($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Filter whereSlug($value)
 * @mixin \Eloquent
 */
class Filter extends Model
{
    public $timestamps = false;

    protected $fillable = [
        'name',
        'slug',
        'order',
        'published',
    ];

    // scopes
    public function scopePublished($query)
    {
        return $query->where('published', true);
    }

    public function scopeOrdered($query)
    {
        return $query->orderBy('order');
    }


    // relations

    /**
     * The photos that belong to the filter
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function photos()
    {
        return $this->belongsToMany(Photo::class);
    }
}
