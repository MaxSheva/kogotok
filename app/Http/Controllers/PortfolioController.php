<?php

namespace App\Http\Controllers;

use App\Filter;
use App\Photo;
use Illuminate\Http\Request;

class PortfolioController extends Controller
{
    public function index(Request $request, $filter_slug = '')
    {

        // id фильтра для фильтрации выборки
        $filter = $filter_slug ? Filter::where('slug', $filter_slug)->firstOrFail()->id : '';


        // если ajax-запрос
        if ($request->ajax()) {
            $offset = $request->offset;  // сколько фото отступаем с начала
            $limit = $request->limit;    // сколько фото выбираем из базы

            // если выбран фильтр
            if ($filter) {
                // коллекция фото со связанными с каждым фото услугами (выбираем фото через фильтр)
                $photos = Filter::find($filter)->photos()->with('services')
                    ->orderBy('created_at', 'desc')
                    ->offset($offset)
                    ->limit($limit)
                    ->get();
            } else {
                $photos = Photo::with('services')
                    ->offset($offset)
                    ->limit($limit)
                    ->get();
            }

            /* Добавляем к каждой услуге модель Категории, к которой принадлежит услуга
             * добавить в каждую модель Service модель Сategory, используя связь
             *
             * это делается для того, чтобы в js получить доступ к имени Категории а не просто к id категории из модели 'Service'
            */

            /*
             * коллекция $photos содержит объекты $photo (модели Photo)
             * каждый объект $photo содержит атрибут #relations - массив связей
             * каждый элемент массива связей (каждая связь) с ключём равным названию связи 'services' содержит
             * или коллекцию объектов связанной модели (при связи 'один ко многим' ) 'Service' или объект связанной мадели (при '...к одному')
             * каждый объект связанной модели 'Service' содержит атрибут #relarions
             * когда мы делаем push в объект $service (Service),
             * добавляемое ($service->category) определяется как объект связи (модель Category - связанная модель с моделью Service)
             * и т.д. в атрибут #relations модели 'Сategory' можно добавить ещё связанные модели, если у 'Сategory' есть ещё какая-нибудь связь
             * получаем дерево связанных моделей
             * */
            foreach ($photos as $photo) {
                foreach ($photo->services as $service) {
                    $service->push($service->category);
                }
            }

            return ($photos->toJson());
        };


        // если обычный запрос

        // если выбран фильтр
        if ($filter) {
            // выборка фото, которые удовлетворяют фильтру, с услугами. (выборка через связь 'фильтру принадлежат много фото')
            $photos = Filter::find($filter)->photos()->with('services')
                ->orderBy('created_at', 'desc')
                ->offset(0)
                ->limit(2)
                ->get();
        } else {
            $photos = Photo::with('services')
                ->offset(0)
                ->limit(2)
                ->get();
        }

        // формируем дерево Photo->Service->Category
        foreach ($photos as $photo) {
            foreach ($photo->services as $service) {
                $service->push($service->category);
            }
        }


        // фильтры для страницы Портфолио
        // все доступные фильтры
        $all_filters = Filter::published()->ordered()->get();

        // фильтры у которых есть фото (только их выводим. пустые фильтры нет смысла выводить)
        $filters = collect();
        foreach ($all_filters as $filter_item) {
            if (count($filter_item->photos))
                $filters->push($filter_item);
        }

//        dd($photos);

        return view('portfolio', compact('filters', 'photos'));
    }
}
