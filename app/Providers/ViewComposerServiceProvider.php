<?php

namespace App\Providers;

use App\Contact;
use App\Status;
use Illuminate\Support\ServiceProvider;

class ViewComposerServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        $this->composeTopPanel();
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /*
     * Compose the top_panel (при рендере top_panel, доступны переменные телефон и город)
     */
    private function composeTopPanel()
    {
        view()->composer('top_panel', function ($view) {
            $phone = Contact::published()->where('type', 'like', 'тел%')->first();

            if ($phone){
                $phone_numeric = preg_replace('/[^0-9]/', '', $phone->data);
            } else {
                $phone_numeric = 0;
            }

            $city = Contact::published()->where('type', 'like', 'гор%')->first();

            $status = Status::published()->first();

            $view->with(compact('phone', 'phone_numeric', 'city', 'status'));
        });
    }
}
