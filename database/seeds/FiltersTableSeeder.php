<?php

use Illuminate\Database\Seeder;

class FiltersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // В database\factories\ModelFactory.php находится фабрика создания Filter
        factory(\App\Filter::class, 14)->create();
    }
}
