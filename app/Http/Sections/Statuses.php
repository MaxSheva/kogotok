<?php

namespace App\Http\Sections;

use AdminColumn;
use AdminDisplay;
use AdminForm;
use AdminFormElement;
use SleepingOwl\Admin\Contracts\Display\DisplayInterface;
use SleepingOwl\Admin\Contracts\Form\FormInterface;
use SleepingOwl\Admin\Contracts\Initializable;
use SleepingOwl\Admin\Section;
use AdminColumnEditable;

/**
 * Class Statuses
 *
 * @property \App\Status $model
 *
 * @see http://sleepingowladmin.ru/docs/model_configuration_section
 */
class Statuses extends Section implements Initializable
{
    /**
     * @see http://sleepingowladmin.ru/docs/model_configuration#ограничение-прав-доступа
     *
     * @var bool
     */
    protected $checkAccess = false;

    /**
     * @var string
     */
    protected $title = 'Статусы';

    /**
     * Если класс реализует интерфейс SleepingOwl\Admin\Contracts\Initializable, то автоматически будет вызван метод initialize, в котром можно поместить код, который нужно выполнить после регистрации класса, например добавление раздела в меню
     * Initialize class.
     */
    public function initialize()
    {
        // Добавление пункта меню, счетчика кол-ва записей в разделе, иконки
        $this->addToNavigation($priority = 200)->setIcon('fa fa-info-circle');
    }

    /**
     * URL по которому будет доступен раздел
     * @var string
     */
    protected $alias = "statuses";

    /**
     * @return DisplayInterface
     */
    public function onDisplay()
    {
        $display = AdminDisplay::table();
        $display->setHtmlAttribute('class', 'table-primary table-hover');
        $display->setColumns([
            AdminColumn::image('img')->setLabel('Аватар'),
            AdminColumnEditable::textarea('text')->setLabel('Текст'),
            AdminColumnEditable::checkbox('published')->setLabel('Show')
        ]);

        return $display;

    }

    /**
     * @param int $id
     *
     * @return FormInterface
     */
    public function onEdit($id)
    {
        return AdminForm::panel()
            ->addBody([
                AdminFormElement::checkbox('published')->setLabel('Публиковать'),
                AdminFormElement::image('img')
                    ->setUploadPath(function () {
                        return 'images/uploads/statuses';
                    })->setLabel('Аватар')->required('Поле обязательно для заполнения'),
                AdminFormElement::text('text')->setLabel('Текст')->setHelpText('короткий текст - несколько слов'),
            ]);
    }

    /**
     * @return FormInterface
     */
    public function onCreate()
    {
        return $this->onEdit(null);
    }

    /**
     * @return void
     */
    public function onDelete($id)
    {
        // remove if unused
    }

    /**
     * @return void
     */
    public function onRestore($id)
    {
        // remove if unused
    }
}
