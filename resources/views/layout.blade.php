<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">

    {{--<meta name="csrf-token" content="{{ csrf_token() }}">--}}

    <title>@yield('title')</title>

    {{-- favicon --}}
    <link rel="shortcut icon" href="{{ asset('images/favicon.png') }}"/>

    {{-- css --}}
    <link rel="stylesheet" href="{{ asset('css/normalize.css') }}">
    <link rel="stylesheet" href="{{ asset('libs/font-awesome/css/font-awesome.min.css') }}">
    <link rel="stylesheet" href="{{ asset('css/style.css') }}">

    {{-- owl-carusel css--}}
    <link rel="stylesheet" href="{{ asset('libs/owlcarousel/owl.carousel.min.css') }}">
    <link rel="stylesheet" href="{{ asset('libs/owlcarousel/owl.theme.default.min.css') }}">

    {{--Magnific Popup core CSS file--}}
    <link rel="stylesheet" href={{ asset('libs/magnific-popup/magnific-popup.css') }}>
</head>
<body>
<div class="wrapper">

@yield('content')


    {{-- FOOTER --}}
    <footer id="footer" class="flex brick">
        <!-- contacts -->
        <div class="footer__contacts flex content_padding">
            <span class="footer__call_to_action">ЗВОНИТЕ </span>
            <span><a href="tel:{{ $phone_numeric }}"> &nbsp;{{ $phone ? $phone->data : 'нипанятна'}}&nbsp;</a></span>
            <span> | Таня&nbsp;</span>
            <span> | {{ $city ? $city->data : 'Земля' }}</span>
        </div>
        <!-- /contacts -->

        <!-- slogan -->
        <div class="about_me__slogan content_padding flex">
            <i class="fa fa-quote-left text_pink"></i>
            <span class="about_me__slogan__first_row"><i class="fa fa-check-square-o text_pink"></i>делаю красиво, качественно</span>
            <span class="about_me__slogan__second_row">с гарантией и <b>с удовольствием</b></span>
        </div>
        <!-- /slogan -->

        <!-- logo + short info -->
        <div class="footer__logo flex content_padding">
            <!-- logo -->
            <div class="footer__logo__title">
                KOGOT<span class="text_pink">OK</span>
            </div>
            <!-- /logo -->
            <div class="long_line"></div>
            <!-- short info -->
            <div class="footer__logo__info">
                МАНИКЮР | НАРАЩИВАНИЕ | ПОКРЫТИЕ | ДИЗАЙН
            </div>
            <!-- /short info -->
        </div>
        <!-- /logo+ short info -->

        <!-- social icons -->
        <div class="footer__social_icon content_padding">
            <a href="#"><i class="fa fa-instagram footer__social_icon content_padding"></i></a>
            <a href="#"><i class="fa fa-facebook footer__social_icon content_padding"></i></a>
            <a href="#"><i class="fa fa-twitter footer__social_icon content_padding"></i></a>
        </div>
        <!-- /social icons -->

        <!-- copyright -->
        <div class="footer__copyright content_padding">
            <i class="fa fa-copyright"></i><span> 2017-{{ date('Y') }} Kogotok</span>
        </div>
        <!-- /copyright -->
    </footer>
    {{-- /FOOTER --}}

</div>

{{--скрипт для всего сайта--}}
<script src="{{ asset('libs/jquery/jquery-3.2.1.min.js') }}"></script>
<script src="{{ asset('libs/owlcarousel/owl.carousel.min.js') }}"></script>
<!-- Magnific Popup core JS file -->
<script src="{{ asset('libs/magnific-popup/magnific-popup.js') }}"></script>
<script src="{{ asset('js/common.js') }}"></script>


{{--скрипты для конкретной страницы--}}
@yield('scripts')

<!-- Yandex.Metrika counter --><!-- /Yandex.Metrika counter -->
<!-- Google Analytics counter --><!-- /Google Analytics counter -->
</body>
</html>