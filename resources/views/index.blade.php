@extends('layout')

@section('title')
    Kogotok | Главная
@endsection

@section('content')

    @include('analyticstracking')

    <!-- FIRST SCREEN -->
    <div class="first_screen flex">

    @include('top_panel')

    <!-- logo + short info -->
        <div id="logo" class="logo flex content_padding">
            <!-- logo -->
            <div class="logo__pic flex">
                K
            </div>
            <div class="logo__title">
                KOGOT<span class="text_pink">OK</span>
            </div>
            <!-- /logo -->
            <div class="long_line"></div>
            <!-- short info -->
            <div class="logo__info">
                МАНИКЮР | НАРАЩИВАНИЕ | ПОКРЫТИЕ | ДИЗАЙН
            </div>
            <!-- /short info -->
        </div>
        <!-- /logo+ short info -->

        <!-- picture -->
        <div id="picture" class="flex">
            <div class="flex picture__main-scroll">
                <div class="btn_row btn_row__main-scroll"></div>
                <div class="btn_row btn_row__main-scroll"></div>
            </div>
        </div>
        <!-- /picture -->

    </div>
    <!-- /FIRST SCREEN -->


    <!-- ABOUT ME SECTION -->
    <section id="about_me" class="flex brick">
        <header class="about_me__header flex">
            <h1>ОБО МНЕ</h1>
            <div class="short_line"></div>
        </header>

        <div class="about_me__bg_girl">
            <div class="about_me__bg_glass flex">
                <!-- social icons -->
                <div class="about_me__social text_pink content_padding flex">
                    <a href="" class="about_me__social__icon flex"><i class="fa fa-instagram"></i></a>
                    <a href="" class="about_me__social__icon flex"><i class="fa fa-facebook"></i></a>
                    <a href="" class="about_me__social__icon flex"><i class="fa fa-twitter"></i></a>
                </div>
                <!-- /social icons -->

                <!--name, prof, slogan-->
                <div class="about_me__brief content_padding flex">
                    <div class="about_me__name">ТАНЯ</div>
                    <div class="about_me__profession">- МАСТЕР МАНИКЮРА -</div>
                    <div class="about_me__slogan content_padding flex">
                        <i class="fa fa-quote-left text_pink"></i>
                        <span class="about_me__slogan__first_row"><i class="fa fa-check-square-o text_pink"></i>делаю красиво, качественно</span>
                        <span class="about_me__slogan__second_row">с гарантией и <b>с удовольствием</b></span>
                    </div>
                </div>
                <!--name, prof, slogan-->

                <!-- experience block -->
                <div id="experience" class="flex content_padding">
                    <!-- icon -->
                    <div class="experience__icon flex">
                        <div class="experience__icon__figure text_pink">{{ date('Y')-2011 }}</div>
                        <span>лет</span>
                    </div>
                    <div class="experience__title">
                        ОПЫТ РАБОТЫ
                    </div>
                    <!-- /icon -->
                    <div class="long_line"></div>
                    <!-- short info -->
                    <div class="experience__info flex">
                        <div class="experience__info__statement">
                            <i class="fa fa-circle-thin"></i>
                            &nbsp;много чудесных работ
                            <a href="{{ route('portfolio') }}" class="call_to_action bg_pink text_white">смотреть
                                работы</a>
                        </div>
                        <div class="experience__info__statement">
                            <i class="fa fa-circle-thin"></i>
                            &nbsp;довольные клиентки
                            {{--<a href="#testimonials" class="call_to_action bg_pink text_white">читать ОТЗЫВЫ</a>--}}
                        </div>
                    </div>
                </div>
                <!-- /experience block -->
            </div>
        </div>
    </section>
    <!-- /ABOUT ME SECTION -->

    <!-- FOR YOU SECTION -->
    <section id="for_you" class="flex brick">
        <header class="for_you__header flex">
            <h1>ДЛЯ ВАС</h1>
            <div class="short_line"></div>
        </header>

        <div class="for_you__bg_face">
            <div class="for_you__bg_glass flex">

                <!-- design block -->
                <div id="design" class="flex content_padding">
                    <!-- icon -->
                    <div class="design__icon content_padding flex">
                        <div class="design__icon__figure">
                            <i class="fa fa-paint-brush"></i>
                        </div>
                    </div>
                    <div class="design__title text_pink">
                        ДИЗАЙН
                    </div>
                    <!-- /icon -->
                    <div class="long_line_white"></div>
                    <!-- short info -->
                    <div class="design__info flex text_white">
                        <div class="design__info__statement">
                            <i class="fa fa-circle-thin"></i>
                            &nbsp;богатый выбор дизайнерских решений
                        </div>
                        <div class="design__info__statement">
                            <i class="fa fa-circle-thin"></i>
                            &nbsp;авторский дизайн
                        </div>
                    </div>
                    <!-- /short info -->
                </div>
                <!-- /design block -->

                <!-- quality block -->
                <div id="quality" class="flex content_padding">
                    <!-- icon -->
                    <div class="quality__icon content_padding flex">
                        <div class="quality__icon__figure">
                            <i class="fa fa-diamond"></i>
                        </div>
                    </div>
                    <div class="quality__title text_pink">
                        КАЧЕСТВО
                    </div>
                    <!-- /icon -->
                    <div class="long_line_white"></div>
                    <!-- short info -->
                    <div class="quality__info flex text_white">
                        <div class="quality__info__statement">
                            <i class="fa fa-circle-thin"></i>
                            &nbsp;материалы премиум класса
                        </div>
                        <div class="quality__info__statement">
                            <i class="fa fa-circle-thin"></i>
                            &nbsp;безопасность
                            <a href="#text-modal" class="popup-modal call_to_action bg_green">узнать больше</a>
                        </div>

                        <div id="text-modal" class="mfp-hide call_to_action__info content_padding text_white flex">
                            <div class="call_to_action__info__header">Безопасность</div>
                            <div>
                                <i class="fa fa-check"></i>
                                <span class="call_to_action__info__item"> выполнение процедур строго по технологии</span>
                            </div>
                            <div>
                                <i class="fa fa-check"></i>
                                <span class="call_to_action__info__item"> чистота в салоне</span>
                            </div>
                            <div>
                                <i class="fa fa-check"></i>
                                <span class="call_to_action__info__item"> опрятность мастера</span>
                            </div>
                            <div>
                                <i class="fa fa-check"></i>
                                <span class="call_to_action__info__item"> чистый маникюрный стол</span>
                            </div>
                            <div>
                                <i class="fa fa-check"></i>
                                <span class="call_to_action__info__item"> обработка рук мастера и клиента спреем-антисептиком</span>
                            </div>
                            <div>
                                <i class="fa fa-check"></i>
                                <span class="call_to_action__info__item"> обработка инструментов</span>
                            </div>
                            <div>
                                <i class="fa fa-check"></i>
                                <span class="call_to_action__info__item"> несколько комплектов инструментов</span>
                            </div>

                            {{--<p><a class="popup-modal-dismiss" href="#">Dismiss</a></p>--}}
                        </div>
                    </div>
                    <!-- /short info -->
                </div>
                <!-- /quality block -->

                <!-- guarantee block -->
                <div id="guarantee" class="flex content_padding">
                    <!-- icon -->
                    <div class="guarantee__icon content_padding flex">
                        <div class="guarantee__icon__figure">
                            <i class="fa fa-certificate"></i>
                        </div>
                    </div>
                    <div class="guarantee__title text_pink">
                        ГАРАНТИЯ
                    </div>
                    <!-- /icon -->
                    <div class="long_line_white"></div>
                    <!-- short info -->
                    <div class="guarantee__info flex text_white">
                        <div class="guarantee__info__statement">
                            <i class="fa fa-circle-thin"></i>
                            &nbsp;гарантирую качество работы
                        </div>
                    </div>
                    <!-- /short info -->
                </div>
                <!-- /guarantee block -->
            </div>
        </div>
    </section>
    <!-- /FOR YOU SECTION -->

    {{--<!-- TESTIMONIALS SECTION -->--}}
    {{--<section id="testimonials" class="flex brick">--}}
        {{--<header class="testimonials__header flex">--}}
            {{--<h1>ОТЗЫВЫ</h1>--}}
            {{--<div class="short_line"></div>--}}
        {{--</header>--}}

        {{--<!-- testimonials carousel -->--}}
        {{--<div class="owl-carousel owl-theme content_padding">--}}

        {{--@foreach($testimonials as $testimonial)--}}
            {{--<!-- testimonial item -->--}}
                {{--<div class="testimonial_item flex">--}}
                    {{--<div class="testimonial_item__photo content_padding">--}}
                        {{--<img src="{{ $testimonial->img }}" alt="photo">--}}
                    {{--</div>--}}
                    {{--<div class="testimonial_item__text content_padding">--}}
                        {{--{{ $testimonial->text }}--}}
                    {{--</div>--}}
                    {{--<div class="long_line"></div>--}}
                    {{--<div class="testimonial_item__name flex">--}}
                        {{--<a href="{{ $testimonial->link }}">--}}
                            {{--<i class="fa fa-instagram fa-lg testimonial_item__social__icon text_pink"></i>--}}
                        {{--</a>--}}
                        {{--&nbsp;{{ $testimonial->name }}--}}
                    {{--</div>--}}
                {{--</div>--}}
                {{--<!-- /testimonial item -->--}}
            {{--@endforeach--}}

        {{--</div>--}}
        {{--<!-- /testimonials carousel -->--}}
    {{--</section>--}}
    {{--<!-- /TESTIMONIALS SECTION -->--}}
@endsection


@section('scripts')

@endsection