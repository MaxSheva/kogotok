@extends('layout')

@section('title')
    Kogotok | Услуги и цены
@endsection

@section('content')

    @include('analyticstracking')

    @include('top_panel')

    <!-- SERVICES and PRICES -->
    <section id="services" class="flex brick">

        <header class="services__header flex">
            <h1>УСЛУГИ и ЦЕНЫ</h1>
            <div class="short_line"></div>
        </header>

        <!-- intro -->
        <div class="services__introduction flex">
            <img src="images/bg_coffee_cut.png" alt="" class="services__introduction__img">
            <div class="services__introduction__statements">
                <div class="services__introduction__statements__item">
                    <i class="fa fa-check-square-o text_rose"></i>
                    <span><i>подобрать дизайн?</i> - <i class="text_light_green">ПОМОГУ</i></span>
                </div>

                <div class="services__introduction__statements__item">
                    <i class="fa fa-check-square-o text_rose"></i>
                    <span><i>новинки?</i> - <i class="text_light_green">ПРЕДЛОЖУ и ПОКАЖУ</i></span>
                </div>

                <div class="services__introduction__statements__item">
                    <i class="fa fa-check-square-o text_rose"></i>
                    <span><i>индивидуальный набор услуг?</i> - <i
                                class="text_light_green">ПОДБЕРУ и ПОРЕКОМЕНДУЮ</i></span>
                </div>
            </div>
        </div>
        <!-- /intro -->

        <!-- services -->
        <div class="services flex">

            @foreach($categories as $category)
                {{--если в категории пусто (нет услуг) - не выводить категорию на экран--}}
                @if(count($category->services))
                    <div class="services__category flex ">

                        <!-- category head-->
                        <div class="services__category__head flex content_padding">
                            <!-- icon -->
                            <div class="category_icon flex">
                                {{--в верхний регистр первую букву названия категории--}}
                                {{ mb_strtoupper(mb_substr($category->name, 0, 1))  }}
                            </div>
                            <!-- /icon -->
                            <!--title-->
                            <div class="category__title">
                                {{ $category->name }}
                            </div>
                            <!--title-->
                        </div>
                        <!-- category head-->

                        <!-- category body-->
                        <div class="services__category__body flex content_padding">

                            {{-- service_item --}}
                            @foreach($category->services as $service)
                                <div class="service_item flex">
                                    <div class="service_head flex">
                                        <div class="service_name flex">
                                            <i class="fa fa-angle-down fa-lg"></i>
                                            <span>&nbsp;{{ $service->name }}</span>
                                        </div>
                                        <div class="service_price">
                                            <i>{{ $service->before_price }}&nbsp;</i>
                                            <span>{{ $service->price }}</span>
                                            <i>грн</i>
                                        </div>
                                    </div>

                                    <div class="service_description">
                                        <i>{{ $service->desc }}</i>
                                    </div>
                                </div>
                            @endforeach
                            {{-- /service_item --}}
                        </div>
                        <!-- /category body-->
                    </div>
                    <!-- /category -->
                @endif
            @endforeach
        </div>
        <!-- /services -->
    </section>
    <!-- /SERVICES and PRICES -->

@endsection


@section('scripts')

@endsection