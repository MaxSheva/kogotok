<?php

namespace App\Http\Controllers;

use App\Category;
use Illuminate\Http\Request;

class ServiceController extends Controller
{
    public function index()
    {
        $categories = Category::published()->ordered()->get();

        return view('services', compact('categories'));
    }
}
