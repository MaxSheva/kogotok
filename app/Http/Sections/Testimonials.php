<?php

namespace App\Http\Sections;

use AdminColumn;
use AdminDisplay;
use AdminForm;
use AdminFormElement;
use App\Testimonial;
use SleepingOwl\Admin\Contracts\Display\DisplayInterface;
use SleepingOwl\Admin\Contracts\Form\FormInterface;
use SleepingOwl\Admin\Contracts\Initializable;
use SleepingOwl\Admin\Section;
use AdminColumnEditable;

/**
 * Class Testimonials
 *
 * @property \App\Testimonial $model
 *
 * @see http://sleepingowladmin.ru/docs/model_configuration_section
 */
class Testimonials extends Section implements Initializable
{
    /**
     * @see http://sleepingowladmin.ru/docs/model_configuration#ограничение-прав-доступа
     *
     * @var bool
     */
    protected $checkAccess = false;

    /**
     * @var string
     */
    protected $title = 'Отзывы';

    /**
     * Если класс реализует интерфейс SleepingOwl\Admin\Contracts\Initializable, то автоматически будет вызван метод initialize, в котром можно поместить код, который нужно выполнить после регистрации класса, например добавление раздела в меню
     * Initialize class.
     */
    public function initialize()
    {
        // Добавление пункта меню, счетчика кол-ва записей в разделе, иконки
        $this->addToNavigation($priority = 300, function () {
            return Testimonial::count();
        })->setIcon('fa fa-commenting');
    }

    /**
     * URL по которому будет доступен раздел
     * @var string
     */
    protected $alias = "testimonials";

    /**
     * @return DisplayInterface
     */
    public function onDisplay()
    {
        $display = AdminDisplay::datatables();
        $display->setHtmlAttribute('class', 'table-primary table-hover');
        $display->setColumns([
            AdminColumn::image('img')->setLabel('Аватар')->setWidth(200),
            AdminColumnEditable::text('name')->setLabel('Имя'),
            AdminColumnEditable::checkbox('published')->setLabel('Show')
        ]);

        return $display;

    }

    /**
     * @param int $id
     *
     * @return FormInterface
     */
    public function onEdit($id)
    {
        return AdminForm::panel()
            ->addBody([
                AdminFormElement::checkbox('published')->setLabel('Публиковать'),
                AdminFormElement::text('name')->setLabel('Имя')->required('Поле обязательно для заполнения'),
                AdminFormElement::image('img')
                    ->setUploadPath(function () {
                        return 'images/uploads/testimonials';
                    })->setLabel('Аватар')->required('Поле обязательно для заполнения'),
                AdminFormElement::text('link')->setLabel('Ссылка на аккаут в Instagram')->setHelpText('https://www.instagram.com/bereztan/?hl=ru'),
                AdminFormElement::textarea('text')->setLabel('Текст')->required('Поле обязательно для заполнения')->setHelpText('кратко, лаконично, 15-30 слов'),
            ]);
    }

    /**
     * @return FormInterface
     */
    public function onCreate()
    {
        return $this->onEdit(null);
    }

    /**
     * @return void
     */
    public function onDelete($id)
    {
        // remove if unused
    }

    /**
     * @return void
     */
    public function onRestore($id)
    {
        // remove if unused
    }
}
