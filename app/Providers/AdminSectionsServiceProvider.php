<?php

namespace App\Providers;

use App\Category;
use App\Contact;
use App\Filter;
use App\Photo;
use App\Service;
use App\Status;
use App\Testimonial;
use App\User;
use SleepingOwl\Admin\Providers\AdminSectionsServiceProvider as ServiceProvider;

class AdminSectionsServiceProvider extends ServiceProvider
{

    /**
     * @var array
     */
    protected $sections = [
        //\App\User::class => 'App\Http\Sections\Users',
        Status::class => 'App\Http\Sections\Statuses',
        Testimonial::class => 'App\Http\Sections\Testimonials',
        Contact::class => 'App\Http\Sections\Contacts',
        Category::class => 'App\Http\Sections\Categories',
        Service::class => 'App\Http\Sections\Services',
        Filter::class => 'App\Http\Sections\Filters',
        Photo::class => 'App\Http\Sections\Photos',
        User::class => 'App\Http\Sections\Users'

    ];

    /**
     * Register sections.
     *
     * @return void
     */
    public function boot(\SleepingOwl\Admin\Admin $admin)
    {
    	//

        parent::boot($admin);
    }
}
