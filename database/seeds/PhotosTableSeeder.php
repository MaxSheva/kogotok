<?php

use Illuminate\Database\Seeder;

class PhotosTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // В database\factories\ModelFactory.php находится фабрика создания Photo
        factory(\App\Photo::class, 6)->create()
            ->each(function ($photo){
                $photo->services()
                    ->attach(\App\Service::published()->inRandomOrder()->limit(rand(1,4))->get());
                $photo->filters()
                    ->attach(\App\Filter::published()->inRandomOrder()->limit(rand(1,4))->get());
            });
    }
}
