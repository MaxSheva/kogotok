<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateServicesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('services', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('category_id')->unsigned()->comment('id категори услуги');
            $table->foreign('category_id')->references('id')->on('categories')->comment('Внешний ключ');
            $table->string('name')->required()->comment('Название услуги');
            $table->string('slug')->unique()->required()->comment('ЧПУ услуги');
            $table->text('desc')->nullable()->comment('Описание услуги');
            $table->string('before_price')->nullable()->comment('если нужно вставить частичку от');
            $table->integer('price')->unsigned()->default(0)->comment('Цена услуги');
            $table->integer('order')->unsigned()->default(0)->comment('Очерёдность вывода услуги');
            $table->boolean('published')->comment('Отображать на сайте');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('services');
    }
}
