<!-- top-panel -->
<div id="top_panel" class="flex content_padding">
    <!-- contacts -->
    <div class="top_panel__contacts">
        <i class="{{ $phone ? $phone->icon : 'fa fa-mobile'}} fa-lg"></i><span><a href="tel:{{ $phone_numeric }}"> {{ $phone ? $phone->data : 'нипанятна'}}</a></span>
        <i class="{{ $city ? $city->icon : 'fa fa-map-marker' }} fa-lg"></i><span> {{ $city ? $city->data : 'Земля' }}</span>
    </div>
    <!-- contacts -->

    <!-- menu button -->
    <div class="top_panel__menu__btn">
        <div class="btn_row"></div>
        <div class="btn_row"></div>
        <div class="btn_row"></div>
    </div>
    <!-- /menu button -->
</div>

<!-- status -->
<div id="status" class="flex">
    <!-- status img -->
    <div class="status__img flex">
        <div class="green_strip flex">
            <img src="{{ $status ? asset($status->img) : asset('/images/uploads/statuses/default.png') }}" alt="фото Тани">
        </div>
    </div>
    <!-- /status img -->

    <!-- status msg -->
    <div class="status__message flex">
        <div class="white_strip flex">
            <p>{{ $status ? $status->text : 'всем бобра' }}</p>
        </div>
    </div>
    <!-- /status msg -->
</div>
<!-- /status -->
<!-- /top-panel -->

<!-- menu list -->
<nav class="top_panel__menu__list content_padding">
    <a href="{{ route('main') }}" class="top_panel__menu__list__link">ГЛАВНАЯ</a>
    <a href="{{ route('services') }}" class="top_panel__menu__list__link">УСЛУГИ и ЦЕНЫ</a>
    <a href="{{ route('portfolio') }}" class="top_panel__menu__list__link">ПОРТФОЛИО</a>
</nav>
<!-- menu list -->
