// $.ajaxSetup({
//     headers: {
//         'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
//     }
// });


$(document).ready(function () {

// портфолио lazy load
    var offset = 2;             // сколько фото отступаем с начала
    var limit = 2;              // сколько фото подгружаем (выбираем из базы)
    var in_process = false;     // чтобы новый ajax запрос не начинался, пока не завершится предыдущий

    $(window).scroll(function () {
        // если доскролили до низа и ajax запрос не выполняется в текущий момент то выполняем его
        if ($(window).scrollTop() + $(window).height() >= ($(document).height() - 600) && !in_process) {

            var url = window.location.pathname;
            // console.log(url);

            $.ajax({
                type: 'GET',
                url: url,
                data: {
                    "offset": offset,
                    "limit": limit,
                },
                beforeSend: function () {
                    $(".loader").addClass('active');
                    in_process = true;
                }
            })

            // действия при завершении ajax-запроса (запрос-ответ)
            // в data такие данные: фото, услуги, связанные с фото и категории, которым услуги пренадлежат
                .done(function (data) {

                    $(".loader").removeClass('active');

                    // превращаем json в массив объектов js
                    data = JSON.parse(data);
                    // console.log(data);

                    // отрисовывам html (если массив объектов не пуст)
                    if (data.length > 0) {
                        $.each(data, function (index, element) {

                            $("#portfolio").append(
                                "<div class='img_wrapper flex'>" +

                                // touch_icon
                                "<span class='touch_icon content_padding flex'><i class='fa fa-bullseye fa-lg'></i></span>" +
                                // /touch_icon

                                // services_cost
                                "<div class='services_cost flex content_padding'>"
                            );

                            // если к фото прикрепили услуги - выводим данные и ВСЕГО грн.
                            if ((element.services).length) {
                                var total_price = 0;
                                $.each(element.services, function (index, service) {

                                        total_price += Number(service.price);

                                        $(".services_cost:last").append(
                                            // services
                                            "<div class='services_cost__service flex'>" +
                                            "<div class='services_cost__service__name'>" +
                                            "<i class='fa fa-check'></i>" +
                                            "<span>" + " " + service.category.name + " " + service.name + "</span>" +
                                            "</div>" +
                                            // "<div class='service_price'>" +
                                            // "<i>" + service.before_price + "</i>" +
                                            // "<span>" + service.price + " грн" + "</span>" +
                                            // "</div>" +
                                            "</div>"
                                            // /services
                                        );
                                    }
                                );

                                // $(".services_cost:last").append(
                                //     "<div class='short_line_white'></div>" +
                                //
                                //     // total_cost
                                //     "<div class='total_cost flex'>" +
                                //     "<div class='total_title'>" +
                                //     "<span> ВСЕГО</span>" +
                                //     "</div>" +
                                //     "<div class='total_price'>" +
                                //     "<span>" + total_price + " грн" + "</span>" +
                                //     "</div>" +
                                //     "</div>"
                                //     //total_cost
                                // )

                            } else {
                                $(".services_cost:last").append(
                                    "<div class='services_cost__alternative_text'>Подробности уточняйте у мастера</div>"
                                );
                            }

                            $(".img_wrapper:last").append(
                                // photo
                                "<img src=" + 'http://' + window.location.host + '/' + element.img + " alt='manicure'>"
                                // /photo
                            );


                            // вешаю обработчики обытия click на вновь добавленные в DOM объекты
                            // показываем стоимость услуг при клике на фото
                            $(".img_wrapper>.touch_icon").click(function () {
                                $(this).siblings(".services_cost").addClass('active');
                            });

                            // скрываем стоимость услуг при клике на блок со стоимостью услуг
                            $(".services_cost").click(function () {
                                $(this).removeClass('active');
                            })
                        });

                        offset += limit;
                        in_process = false;
                    }

                })

                // если что-то пошло не так :)
                .fail(function () {
                    alert("В жизни так бываем");
                })

        }
    })
    ;


})
;