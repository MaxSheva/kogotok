<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use SleepingOwl\Admin\Traits\OrderableModel;

/**
 * App\Contact
 *
 * @property int $id
 * @property string $type Тип контакта: адрес, телефон, email, viber, skype...
 * @property string|null $icon font-awesome иконка типа контакта: адрес - fa-map-marker, телефон - fa-phone...
 * @property string $data Содержание контакта: +380(50) 333-44-55, mail@gmail.com, The White House 1600 Pennsylvania Avenue NW Washington, DC 20500
 * @property int $order Очерёдность вывода контактов (для возможности сортировки)
 * @property int $published Отображать на сайте
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Contact ordered()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Contact published()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Contact whereData($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Contact whereIcon($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Contact whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Contact whereOrder($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Contact wherePublished($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Contact whereType($value)
 * @mixin \Eloquent
 */
class Contact extends Model
{
    public $timestamps = false;

    protected $fillable = [
        'type',
        'icon',
        'data',
        'order',
        'published',
    ];

    public function scopePublished($query)
    {
        return $query->where('published', true);
    }

    public function scopeOrdered($query)
    {
        return $query->orderBy('order');
    }

}
