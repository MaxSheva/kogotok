<?php

use Illuminate\Database\Seeder;

class StatusesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // В database\factories\ModelFactory.php находится фабрика создания Producer
        factory(\App\Status::class, 2)->create();
    }
}
