<?php

namespace App\Http\Sections;

use AdminColumn;
use AdminDisplay;
use AdminForm;
use AdminFormElement;
use App\Contact;
use SleepingOwl\Admin\Contracts\Display\DisplayInterface;
use SleepingOwl\Admin\Contracts\Form\FormInterface;
use SleepingOwl\Admin\Contracts\Initializable;
use SleepingOwl\Admin\Section;
use AdminColumnEditable;

/**
 * Class Contacts
 *
 * @property \App\Contact $model
 *
 * @see http://sleepingowladmin.ru/docs/model_configuration_section
 */
class Contacts extends Section implements Initializable
{
    /**
     * @see http://sleepingowladmin.ru/docs/model_configuration#ограничение-прав-доступа
     *
     * @var bool
     */
    protected $checkAccess = false;

    /**
     * @var string
     */
    protected $title = 'Контакты';

    /**
     * Если класс реализует интерфейс SleepingOwl\Admin\Contracts\Initializable, то автоматически будет вызван метод initialize, в котром можно поместить код, который нужно выполнить после регистрации класса, например добавление раздела в меню
     * Initialize class.
     */
    public function initialize()
    {
        // Добавление пункта меню, счетчика кол-ва записей в разделе, иконки
        $this->addToNavigation($priority = 400, function () {
            return Contact::count();
        })->setIcon('fa fa-globe');
    }

    /**
     * URL по которому будет доступен раздел
     * @var string
     */
    protected $alias = "contacts";

    /**
     * @return DisplayInterface
     */
    public function onDisplay()
    {
        $display = AdminDisplay::datatables();
        $display->setHtmlAttribute('class', 'table-primary table-hover');

        $display->setColumns([
            AdminColumn::text('id')->setLabel('#'),
            AdminColumnEditable::text('type')->setLabel('Тип'),
//            AdminColumnEditable::text('icon')->setLabel('Иконка'),
            AdminColumnEditable::text('data')->setLabel('Контактная информация'),
            AdminColumnEditable::text('order')->setLabel('№ п/п'),
            AdminColumnEditable::checkbox('published')->setLabel('Show')
        ]);

        return $display;
    }

    /**
     * @param int $id
     *
     * @return FormInterface
     */
    public function onEdit($id)
    {
        return AdminForm::panel()
            ->addBody([
                AdminFormElement::checkbox('published')->setLabel('Публиковать'),
                AdminFormElement::number('order')->setLabel('Очерёдность')->required('Поле обязательно для заполнения')->setDefaultValue(0),
                AdminFormElement::text('type')->setLabel('Тип')->required('Поле обязательно для заполнения')->setHelpText('телефон, город, instagram, skype, viber...'),
                AdminFormElement::text('icon')->setLabel('Иконка')->required('Поле обязательно для заполнения'),
                AdminFormElement::text('data')->setLabel('Контактная информация')->required(),
            ]);
    }

    /**
     * @return FormInterface
     */
    public function onCreate()
    {
        return $this->onEdit(null);
    }

    /**
     * @return void
     */
    public function onDelete($id)
    {
        // remove if unused
    }

    /**
     * @return void
     */
    public function onRestore($id)
    {
        // remove if unused
    }
}
