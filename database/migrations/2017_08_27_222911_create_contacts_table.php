<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateContactsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('contacts', function (Blueprint $table) {
            $table->increments('id');
            $table->string('type')->required()->comment('Тип контакта: адрес, телефон, email, viber, skype...');
            $table->string('icon')->nullable()->comment('font-awesome иконка типа контакта: адрес - fa-map-marker, телефон - fa-phone...');
            $table->string('data')->unique()->required()->comment('Содержание контакта: +380(50) 333-44-55, mail@gmail.com, The White House 1600 Pennsylvania Avenue NW Washington, DC 20500');
            $table->integer('order')->unsigned()->default(0)->comment('Очерёдность вывода контактов (для возможности сортировки)');
            $table->boolean('published')->comment('Отображать на сайте');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('contacts');
    }
}
